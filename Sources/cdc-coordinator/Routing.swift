//
//  Routing.swift
//  
//
//  Created by Dary Azhari on 07/07/24.
//

import UIKit

/**
 `Routing`

 A protocol responsible for navigation. It handles the events for the  `UINavigationController`
 It also wraps and delegates the responsibility for back button events back to the coordinator
 e.g deallocation of a child coordinator

 - Note:
 Typically, there is one router for each horizontal (push) flow and may be shared by child coordinators via dependency injection Every new vertical flow (modal presenting) will require instantiating a new router
 */

public protocol Routing: AnyObject, Presentable {
    var rootViewController: UIViewController? { get }
    var navigationController: UINavigationController { get }

    func popModule(animated: Bool)
    func popToRootModule(animated: Bool)
    func present(_ module: Presentable, animated: Bool)
    func setRootModule(_ module: Presentable, hideNavBar: Bool)
    func dismissModule(animated: Bool, completion: (() -> Void)?)
    func push(_ module: Presentable, animated: Bool, completion: (() -> Void)?)
}

public extension Routing {
    func setRootModule(_ module: Presentable) {
        setRootModule(module, hideNavBar: false)
    }
}
