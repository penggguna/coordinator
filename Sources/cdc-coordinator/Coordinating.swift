//
//  Coordinating.swift
//
//
//  Created by Dary Azhari on 07/07/24.
//

import Foundation

/**
 `Coordinating`

 A protocol responsible for driving the application business logic flow
 It can also handle deeplinks, push notifications, user activity in AppDelegate etc

 - Note:
 Deeplinking can be achieved by delegating to a deeplinking module e.g `DeeplinkService`
 User activity can be handled by building a deeplink from the acticity and then delegating once more
 */

public protocol Coordinating: AnyObject {
    func start()

    var childCoordinators: Set<Coordinator> { get set }
}

/**
 `CoordinatorPresentable`

 A protocol composition enabling adopting modules to act as presentable coordinators
 This is achieved by implemeting the `Presentable` protocol, which resolves the coordinator to a controller
 */

public protocol CoordinatorPresentable: Coordinating, Presentable {}

/**
 `RoutingCoordinatorPresentable`

 A protocol that enables a coordinator to have ability to delegate navigation to a router

 - Note:
 Not all coordinators need to handle navigation
 Those that handle navigation will need to adopt this protocol
 Those that don't need to handle navigation can adopt `CoordinatorPresentable`
 However still, if a coordinator does not need to navigate or be presentable, it can simply adopt the root `Coordinating` protocol
 */

public protocol RoutingCoordinatorPresentable: CoordinatorPresentable {
    var router: Routing { get }
}

