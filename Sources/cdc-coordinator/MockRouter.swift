//
//  MockRouter.swift
//  
//
//  Created by Dary Azhari on 07/07/24.
//

import UIKit

public final class MockRouter: Routing {
    public struct RecordedInvocations {
        public var popModule: [Bool] = []
        public var popToRootModule: [Bool] = []
        public var present: [(module: Presentable, animated: Bool)] = []
        public var dismissModule: [(animated: Bool, completion: (() -> Void)?)] = []
        public var push: [(module: Presentable, animated: Bool, completion: (() -> Void)?)] = []
        public var setRootModule: [(module: Presentable, hideNavBar: Bool)] = []

        public init() {}
    }

    public struct StubbedResults {
        public var toPresentable = UIViewController()
        public var rootViewController = UIViewController()
        public var navigationController = UINavigationController()

        public init() {}
    }

    public var rootViewController: UIViewController? {
        stubbedResults.rootViewController
    }

    public private(set) var didReadNavigationController = false
    public var navigationController: UINavigationController {
        didReadNavigationController = true
        return stubbedResults.navigationController
    }

    public var recordedInvocations = RecordedInvocations()
    public var stubbedResults = StubbedResults()

    public init() {}

    public func popModule(animated: Bool) {
        recordedInvocations.popModule.append(animated)
    }

    public func popToRootModule(animated: Bool) {
        recordedInvocations.popToRootModule.append(animated)
    }

    public func present(_ module: Presentable, animated: Bool) {
        recordedInvocations.present.append((module, animated))
    }

    public func dismissModule(animated: Bool, completion: (() -> Void)?) {
        recordedInvocations.dismissModule.append((animated, completion))
    }

    public func push(_ module: Presentable, animated: Bool, completion: (() -> Void)?) {
        recordedInvocations.push.append((module, animated, completion))
    }

    public func setRootModule(_ module: Presentable, hideNavBar: Bool) {
        recordedInvocations.setRootModule.append((module, hideNavBar))
    }

    public func toPresentable() -> UIViewController {
        stubbedResults.toPresentable
    }
}

public extension MockRouter {
    func completeDismissModule(at index: Int = 0) {
        recordedInvocations.dismissModule[index].completion?()
    }

    func completePushModule(at index: Int = 0) {
        recordedInvocations.push[index].completion?()
    }
}
