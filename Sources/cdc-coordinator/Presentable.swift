//
//  Presentable.swift
//
//
//  Created by Dary Azhari on 07/07/24.
//

import UIKit

/**
 `Presentable`
 A protocol for resolving a view to be presented
 When a `Router` implements  it, resolves to the root controller from which navigation takes place
 When a `Coordinator` implements it, it resolves to the controller configured by that coordinator

 - Note:
 When a coordinator manages several views, the current controller determined by the state machine is returned
 e.g a `LoginCoordinator` may resolve to a captcha, 2FA or PIN screen depending on login response
 */

public protocol Presentable {
    func toPresentable() -> UIViewController
}

// MARK: UIViewController

extension UIViewController: Presentable {
    public func toPresentable() -> UIViewController {
        return self
    }
}

