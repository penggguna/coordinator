//
//  Router.swift
//
//
//  Created by Dary Azhari on 07/07/24.
//

import UIKit

public final class Router: NSObject, Routing, UINavigationControllerDelegate {
    public var hasRootController: Bool {
        return rootViewController != nil
    }

    public var rootViewController: UIViewController? {
        return navigationController.viewControllers.first
    }

    public let navigationController: UINavigationController
    private var completions: [UIViewController: () -> Void]

    public init(navigationController: UINavigationController = UINavigationController()) {
        completions = [:]
        self.navigationController = navigationController

        super.init()
        self.navigationController.delegate = self
    }

    /**
      A method that presents the supplied `module`

      A `module` can be:
      - A router: which resolves to root controller
      - A coordinator: which resolves to the controller held by that coordinator
      - A view controller

      Since a module is `Presentable`,  `module.toPresentable()` performs the resolution
     */
    public func present(_ module: Presentable,
                        animated: Bool = true) {
        navigationController.present(
            module.toPresentable(),
            animated: animated,
            completion: nil
        )
    }

    public func dismissModule(animated: Bool = true,
                              completion: (() -> Void)? = nil) {
        navigationController.dismiss(
            animated: animated,
            completion: completion
        )
    }

    public func push(_ module: Presentable,
                     animated: Bool = true,
                     completion: (() -> Void)? = nil) {
        let controller = module.toPresentable()

        // Avoid pushing UINavigationController onto stack
        // Typically happens when you set a root module\
        // on a horizontal flow that already has a root module e.g TabBar controller
        if controller is UINavigationController {
            print("Navigation error: Attempted to push UINavigationController")
            return
        }

        if let completion = completion {
            completions[controller] = completion
        }

        navigationController.pushViewController(
            controller,
            animated: animated
        )
    }

    public func popModule(animated: Bool = true) {
        if let controller = navigationController.popViewController(animated: animated) {
            runCompletion(for: controller)
        }
    }

    /**
      This method sets the supplied module as the rootview for the coordinator
      This will be a navigation controller at the beginning of a flow
     */
    public func setRootModule(_ module: Presentable,
                              hideNavBar: Bool = false) {
        // Call all completions so all coordinators can be deallocated
        completions.forEach { $0.value() }

        navigationController.setViewControllers(
            [module.toPresentable()],
            animated: false
        )

        navigationController.setNavigationBarHidden(
            hideNavBar,
            animated: false
        )
    }

    public func popToRootModule(animated: Bool) {
        let controllers = navigationController
            .popToRootViewController(animated: animated)
        controllers?.forEach { runCompletion(for: $0) }
    }

    fileprivate func runCompletion(for controller: UIViewController) {
        guard let completion = completions[controller] else { return }
        completion()
        completions.removeValue(forKey: controller)
    }
}

// MARK: Presentable

public extension Router {
    func toPresentable() -> UIViewController {
        navigationController
    }
}

// MARK: UINavigationControllerDelegate

public extension Router {
    /**
      A UINavigationControllerDelegate method useful for handling back navigation
      It intercepts back button actions and runs the corresponding completion handler for the view controller that was popped
      It does nothing if a forward navigation
     */
    func navigationController(_ navigationController: UINavigationController,
                              didShow _: UIViewController,
                              animated _: Bool) {
        // Ensure the view controller is popped from stack
        guard
            let poppedViewController = navigationController
            .transitionCoordinator?
            .viewController(forKey: .from),
            !navigationController
            .viewControllers
            .contains(poppedViewController)
        else { return }

        runCompletion(for: poppedViewController)
    }
}
