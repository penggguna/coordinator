//
//  Coordinator.swift
//
//
//  Created by Dary Azhari on 07/07/24.
//

import UIKit

/**
 `Coordinator`

 The base class for coordinators

 - Note:
 Deeplinking can be achieved by delegating to a deeplinking module e.g `DeeplinkService`
 User activity can be handled by building a deeplink from the acticity and then delegating once more
 */

open class Coordinator: NSObject, RoutingCoordinatorPresentable {
    open var router: Routing
    public var childCoordinators: Set<Coordinator> = []

    public init(router: Routing) {
        self.router = router
    }

    /**
      These `start` methods provide default implementation for the coordinator that does nothing

      - Note:
      Coordinators subclassing this base coordinator will have to override these func to add configuration
     */
    open func start() {}

    /**
      This base class method sets the presentable to the rootview of the application
      This will always be a navigation controller (for one of the tabbars in our setup)

      - Note:
      Coordinators subclassing this base coordinator will have to override this func to return the relevant controller
     */
    open func toPresentable() -> UIViewController {
        return router.toPresentable()
    }
}

public extension Coordinator {
    func addChild(_ coordinator: Coordinator) {
        childCoordinators.insert(coordinator)
    }

    func removeChild(_ coordinator: Coordinator?) {
        guard let coordinator = coordinator else { return }
        childCoordinators.remove(coordinator)
    }
}

